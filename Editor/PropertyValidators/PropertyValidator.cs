﻿using UnityEditor;

namespace DenchyaknowAttributes.Editor
{
    public abstract class PropertyValidator
    {
        public abstract void ValidateProperty(SerializedProperty property);
    }
}
