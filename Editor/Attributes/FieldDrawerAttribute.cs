﻿using System;

namespace DenchyaknowAttributes.Editor
{
    public class FieldDrawerAttribute : BaseAttribute
    {
        public FieldDrawerAttribute(Type targetAttributeType) : base(targetAttributeType)
        {
        }
    }
}
