﻿using System;

namespace DenchyaknowAttributes.Editor
{
    public interface IAttribute
    {
        Type TargetAttributeType { get; }
    }
}
