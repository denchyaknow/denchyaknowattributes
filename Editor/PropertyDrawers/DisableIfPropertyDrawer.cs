using UnityEditor;

namespace DenchyaknowAttributes.Editor
{
    [PropertyDrawer(typeof(DisableIfAttribute))]
    public class DisableIfPropertyDrawer : EnableIfPropertyDrawer
    {
    }
}
