﻿using UnityEditor;

namespace DenchyaknowAttributes.Editor
{
    public abstract class PropertyDrawCondition
    {
        public abstract bool CanDrawProperty(SerializedProperty property);
    }
}
