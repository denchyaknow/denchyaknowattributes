using System;

namespace DenchyaknowAttributes
{
    public abstract class MetaAttribute : NaughtyAttribute
    {
        public int Order { get; set; }
    }
}
