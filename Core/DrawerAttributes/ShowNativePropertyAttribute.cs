﻿using System;
using UnityEngine;

namespace DenchyaknowAttributes
{
    [AttributeUsage(AttributeTargets.Property, AllowMultiple = false, Inherited = true)]
    public class ShowNativePropertyAttribute : DrawerAttribute
    {

    }
}
